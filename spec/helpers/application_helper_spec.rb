require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe "#full_title" do
    subject { full_title(page_title: page_title) }

    context "引数が空の場合" do
      let(:page_title) { "" }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数がnilの場合" do
      let(:page_title) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数が空またはnil以外の場合" do
      let(:page_title) { "Ruby on Rails" }

      it { is_expected.to eq "Ruby on Rails - BIGBAG Store" }
    end
  end
end
