require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    get potepan_product_path(id: product.id)
  end

  it "responds successfully" do
    aggregate_failures do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end
  end

  it "productが取得できていること" do
    expect(response.body).to include product.name
  end

  it "related_productsが取得できていること" do
    RELATED_PRODUCTS_MAX_DISPLAY_COUNT.times do |n|
      expect(response.body).to include related_products.reverse[n].name
    end
  end

  it "関連商品がRELATED_PRODUCTS_MAX_DISPLAY_COUNTより多く取得されていないこと" do
    expect(Capybara.string(response.body)).
      to have_selector ".productBox", count: RELATED_PRODUCTS_MAX_DISPLAY_COUNT
  end
end
