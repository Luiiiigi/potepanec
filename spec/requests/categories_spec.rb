require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:base_product, name: "TOTE", price: 15.99, taxons: [taxon]) }

  before do
    get potepan_category_path(id: taxon.id)
  end

  it "responds successfully" do
    aggregate_failures do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end
  end

  it "taxonomyが取得できていること" do
    expect(response.body).to include taxonomy.name
  end

  it "taxonが取得できていること" do
    expect(response.body).to include taxon.name
  end

  it "productが取得できていること" do
    expect(response.body).to include product.name
  end
end
