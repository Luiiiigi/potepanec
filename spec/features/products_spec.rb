require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: "Category") }
  let!(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "TOTE", taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  scenario "商品情報が表示されていることを検証する" do
    visit potepan_product_path(id: product.id)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
    within ".productsContent" do
      RELATED_PRODUCTS_MAX_DISPLAY_COUNT.times do |n|
        expect(page).to have_content related_products.reverse[n].name
        expect(page).to have_content related_products.reverse[n].price
      end
    end
  end

  scenario "Homeリンクをクリックしてindexページに移動する" do
    visit potepan_product_path(id: product.id)
    expect(page).to have_link "Home"
    within ".breadcrumb" do
      click_on "Home"
    end
    expect(current_path).to eq potepan_path
  end

  scenario "一覧ページへ戻るをクリックして商品が属するカテゴリー一覧に戻る" do
    visit potepan_category_path(id: taxon.id)
    click_on product.name
    expect(current_path).to eq potepan_product_path(id: product.id)
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(id: taxon.id)
  end

  scenario "関連商品をクリックして商品詳細ページに移動する" do
    visit potepan_product_path(id: product.id)
    within ".productsContent" do
      expect(page).to have_selector ".productBox", count: RELATED_PRODUCTS_MAX_DISPLAY_COUNT
      expect(page).to have_content related_products.last.name
      expect(page).to have_content related_products.last.price
      expect(page).to have_no_content product.name
    end
    click_on related_products.last.name
    expect(current_path).to eq potepan_product_path(id: related_products.last.id)
  end
end
