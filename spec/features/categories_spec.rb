require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: "Category") }
  let!(:other_taxonomy) { create(:taxonomy, name: "Brand") }
  let!(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:other_taxon) do
    create(
      :taxon,
      name: "Rails",
      taxonomy: other_taxonomy,
      parent_id: other_taxonomy.root.id
    )
  end
  let!(:product) { create(:base_product, name: "TOTE", price: 15.99, taxons: [taxon]) }
  let!(:other_product) { create(:base_product, name: "Mugs", price: 13.99, taxons: [other_taxon]) }

  scenario "ユーザーが選択した商品カテゴリーに紐づく商品一覧が表示されていることを検証する" do
    visit potepan_category_path(id: taxonomy.root.id)
    expect(page).to have_title "#{taxon.root.name} - BIGBAG Store"
    expect(page).to have_content taxonomy.name
    expect(page).to have_content other_taxonomy.name
    within "#category-#{taxon.root.id}" do
      expect(page).to have_link "#{taxon.name} (#{taxon.all_products.count})"
      expect(page).to have_no_link "#{other_taxon.name} (#{other_taxon.all_products.count})"
    end
    click_on "#{taxon.name} (#{taxon.all_products.count})"
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_no_content other_product.name
    expect(page).to have_no_content other_product.price
  end

  scenario "商品情報をクリックして商品詳細ページに移動する" do
    visit potepan_category_path(id: taxon.id)
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    click_on product.name
    expect(current_path).to eq potepan_product_path(id: product.id)
  end
end
