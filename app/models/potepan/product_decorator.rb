module Potepan::ProductDecorator
  def self.prepended(base)
    base.scope :related_products, ->(product) {
      Spree::Product.in_taxons(product.taxons).
        where.not(id: product.id).
        distinct.
        order(id: :desc)
    }
  end

  Spree::Product.prepend self
end
