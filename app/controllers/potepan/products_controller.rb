class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).
      limit(RELATED_PRODUCTS_MAX_DISPLAY_COUNT).
      includes(master: [:default_price, :images])
  end
end
